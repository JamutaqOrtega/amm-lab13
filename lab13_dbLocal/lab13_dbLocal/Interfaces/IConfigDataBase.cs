﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab13_dbLocal.Interfaces
{
    public interface IConfigDataBase
    {
        string GetFullPath(string databaseFileName);
    }
}
