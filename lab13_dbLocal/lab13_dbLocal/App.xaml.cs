﻿using System;
using Xamarin.Forms;
using lab13_dbLocal.DataContext;
using lab13_dbLocal.Interfaces;
using lab13_dbLocal.Views;

namespace lab13_dbLocal
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            GetContext().Database.EnsureCreated();
            MainPage = new NavigationPage(new AlbumesPage());
        }

        public static AppDbContext GetContext()
        {
            string DbPath = DependencyService.Get<IConfigDataBase>().GetFullPath("efCore.db");

            return new AppDbContext(DbPath);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
