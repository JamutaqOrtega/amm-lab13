﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab13_dbLocal.Models
{
    public class Artista
    {
        public int ArtistaID { get; set; }
        public string Nombre { get; set; }
    }
}