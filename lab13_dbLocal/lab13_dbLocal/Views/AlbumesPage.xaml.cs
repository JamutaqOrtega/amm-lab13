﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using lab13_dbLocal.ViewModels;

namespace lab13_dbLocal.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlbumesPage : ContentPage
    {
        public AlbumesPage()
        {
            InitializeComponent();
            this.BindingContext = new AlbumesViewModel();
        }
    }
}