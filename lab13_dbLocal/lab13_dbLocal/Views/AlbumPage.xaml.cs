﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using lab13_dbLocal.ViewModels;

namespace lab13_dbLocal.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlbumPage : ContentPage
    {
        public AlbumPage()
        {
            InitializeComponent();
            this.BindingContext = new AlbumViewModel();
        }
    }
}