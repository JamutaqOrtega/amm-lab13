﻿using System;
using System.IO;
using Xamarin.Forms;
using lab13_dbLocal.Interfaces;
using lab13_dbLocal.iOS.Implementations;

[assembly: Dependency(typeof(ConfigDataBase))]
namespace lab13_dbLocal.iOS.Implementations
{
    public class ConfigDataBase : IConfigDataBase
    {
        public string GetFullPath(string databaseFileName)
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library", databaseFileName);
        }
    }
}